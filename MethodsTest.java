public class MethodsTest{
	public static void main(String[] args){
		int x = 10;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(x+50);
		methodTwoInputNoReturn(2,3.5);
		int z = methodNoInputReturnInt();
		double squared = sumSquaredRoot(6,3);
		System.out.println(squared);
		String s1 = "hello";
		String s2 = "goodbye";
		s1.length();
		s2.length();
		System.out.println(s1.length());
		System.out.println(s2.length());
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		int instance = sc.addTwo(50);
		System.out.println("this prints part 2, iii, #2: "+instance);
	}
	public static void methodNoInputNoReturn(){
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 50;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int youCanCallThisWhateverYouLike){
		System.out.println("Inside the method one input no return");
		System.out.println(youCanCallThisWhateverYouLike);
	}
	public static void methodTwoInputNoReturn(int input1, double input2){
		
	}
	public static int methodNoInputReturnInt(){
		return 6;
	}
	public static double sumSquaredRoot(int squareInput1, int squareInput2){
		double squaredValue = Math.sqrt(squareInput1 + squareInput2);
		return squaredValue;
	}
}